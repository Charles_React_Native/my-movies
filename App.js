import React from "react";
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from "react-navigation";
import HomeScreen from "./src/screens/home/home";
import MovieDetailsScreen from "./src/screens/movie-details/movie-details";
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import SettingsScreen from "./src/screens/settings/settings";

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  Details: {
    screen: MovieDetailsScreen
  }
}, {
  initialRouteName: 'Home'
});

const SettingsNAvigator = createStackNavigator({
  Settings: {
    screen: SettingsScreen
  }
});

const TabNavigator = createBottomTabNavigator(
  {
    Home: AppNavigator,
    Settings: SettingsNAvigator,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let icon = null;
        if (routeName === 'Home') {
          icon = <Icon name="ios-home" size={30} color={focused? '#4F8EF7' : 'gray'} />;
        } else if (routeName === 'Settings') {
          icon =  <Icon name="ios-settings" size={30} color={focused? '#4F8EF7' : 'gray'} />;
        }
        return (
          <View style={{paddingTop: 2}}>
            {icon}
          </View>
        )
      },
    }),
    tabBarOptions: {
      activeTintColor: '#4F8EF7',
      inactiveTintColor: 'gray',
    },
  }
);

const AppContainer = createAppContainer(TabNavigator);
export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}